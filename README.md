# Example application of the Electron Base framework

![Logo Genie23](assets/imgs/logos/genie23.text.svg)

The purpose of this example application is to demonstrate the functionality of the framework [Electron Base](https://www.npmjs.com/package/@genie23/electron-base) in a very simple
application.

> :warning: Project under development ! Do not use for the moment !