$(() => {
    window.preload.ipc.appReady(() => {
        window.home.ipc.getNewTitle(
            (event, newTitle) => {
                $("h1").attr("data-i18n", newTitle).localize();
            }
        );

        $("#changeTitle").on("click", () => {
            console.log("Ask new title");
            window.home.ipc.onGetNewTitle();
            window.preload.ipc.onShowSuccess("Titre changé", "Le titre a été changé avec succès");
        });

        console.log("Home controller loaded");
    });
});
