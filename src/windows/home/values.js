const path = require("path");
const {Config} = require("@genie23/electron-base");
const config = require(Config.projectPath("package.json"));

module.exports = () => {
    return {
        appName: config.productName,
    };
}