const {ipcRenderer, contextBridge} = require("electron");

require("@genie23/electron-base/preloads");

console.log("Preload controller.js", "Window", "#" + window.id);

contextBridge.exposeInIsolatedWorld(window.id, "home", {
    "ipc": {
        "getNewTitle": callback => ipcRenderer.on("onGetNewTitle", callback),
        "onGetNewTitle": () => ipcRenderer.send("getNewTitle")
    }
});
