const {app, nativeImage} = require("electron");
const path = require("path");
const {Config, WindowManager, WindowAbout} = require("@genie23/electron-base");

module.exports = i18nextMainBackend => {
    const isMac = process.platform === "darwin";

    return [
        ...(isMac ? [
            {
                label: app.name,
                submenu: [
                    {
                        label: i18nextMainBackend.t("quitItemMenu"),
                        icon: nativeImage.createFromPath(Config.projectPath("assets/imgs/menu/file/close.24x24.png")),
                        role: "close"
                    }
                ]
            }
        ] : []),
        {
            label: i18nextMainBackend.t("fileMenu"),
            submenu: [
                {
                    label: i18nextMainBackend.t("languageSubmenu"),
                    icon: nativeImage.createFromPath(Config.projectPath("assets/imgs/menu/file/language.24x24.png")),
                    submenu: [
                        {
                            label: "English",
                            accelerator: "CmdOrCtrl+1",
                            icon: nativeImage.createFromPath(Config.projectPath("assets/imgs/menu/file/lang/en-flag.24x24.png")),
                            click() {
                                i18nextMainBackend.changeLanguage("en");
                            }
                        },
                        {
                            label: "Français",
                            accelerator: "CmdOrCtrl+2",
                            icon: nativeImage.createFromPath(Config.projectPath("assets/imgs/menu/file/lang/fr-flag.24x24.png")),
                            click() {
                                i18nextMainBackend.changeLanguage("fr");
                            }
                        }
                    ]
                },
                ...(!isMac ? [
                    {role: "separator"},
                    {
                        label: i18nextMainBackend.t("quitItemMenu"),
                        accelerator: "Alt+F4",
                        icon: nativeImage.createFromPath(Config.projectPath("assets/imgs/menu/file/close.24x24.png")),
                        role: "quit"
                    }
                ] : [])
            ]
        },
        {
            label: "?",
            submenu: [
                {
                    label: i18nextMainBackend.t("aboutItemMenu"),
                    accelerator: "F1",
                    icon: nativeImage.createFromPath(Config.projectPath("assets/imgs/menu/help/about.24x24.png")),
                    click() {
                        const about = WindowManager.create(new WindowAbout());
                        about.focus();
                    }
                }
            ]
        },
        ...(Config.isDebug ? [
            {
                label: i18nextMainBackend.t("debugMenu"),
                submenu: [
                    {
                        label: i18nextMainBackend.t("debugToolsItemMenu"),
                        icon: nativeImage.createFromPath(Config.projectPath("assets/imgs/menu/debug/tools.24x24.png")),
                        role: "toggleDevTools"
                    },
                    {role: "separator"},
                    {
                        label: i18nextMainBackend.t("reloadItemMenu"),
                        icon: nativeImage.createFromPath(Config.projectPath("assets/imgs/menu/debug/reload.24x24.png")),
                        role: "reload"
                    },
                    {
                        label: i18nextMainBackend.t("forceReloadItemMenu"),
                        icon: nativeImage.createFromPath(Config.projectPath("assets/imgs/menu/debug/force-reload.24x24.png")),
                        role: "forceReload"
                    }
                ]
            }
        ] : []),
    ]
};
