const {WindowBase, NotificationManager} = require("@genie23/electron-base");

class WindowHome extends WindowBase {
    constructor() {
        super(
            {
                layoutName: "main",
                menuName: "main",
                viewName: "home",
                title: "Electron Base",
                show: false,
                ipcListeners: {
                    "getNewTitle": event => {
                        console.log("Send new title");
                        event.sender.send("onGetNewTitle", "alternativeTitle");
                    },
                    "showSuccess": (_, title, body) => {
                        NotificationManager.showSuccess(title, body, () => {
                            console.log("Notification clicked");
                        });
                    }
                },
            }
        );
    }
}

module.exports = WindowHome;
