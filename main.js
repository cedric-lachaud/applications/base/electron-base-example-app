require("@genie23/electron-base/config")({
    layoutPath: "src/windows/layouts/{{layout}}.html",
    viewPath: "src/windows/{{name}}/content.html",
    stylePath: "src/windows/{{name}}/style.css",
    preloadPath: "src/windows/{{name}}/preload.js",
    controllerPath: "src/windows/{{name}}/controller.js",
    i18nValuesPath: "src/windows/{{name}}/values.js",
    menuPath: "src/windows/menus/{{menu}}.js",
    contextMenuPath: "src/windows/contextMenus/{{menu}}.js",
    i18nDirPath: "assets/i18n",
    i18nTranslationPath: "assets/i18n/{{lng}}/{{ns}}.json",
    iconPath: "assets/imgs/icons/icon.png",
    additionalCredits: [
        {
            name: "Electron Base",
            href: "https://www.npmjs.com/package/@genie23/electron-base",
            text: "Créé par Cédric LACHAUD"
        },
        {
            i18nName: "testItem",
            href: "https://www.npmjs.com/package/@genie23/electron-base",
            i18nText: "testTitle"
        },
    ]
});

const unhandled = require("electron-unhandled");
const {app, dialog, BrowserWindow, ipcMain} = require("electron");
const path = require("path");
const {AgreeLicense, Config, i18nextBackend, i18nextMainBackend, WindowManager} = require("@genie23/electron-base");

const WindowHome = require(Config.projectPath("src/windows/WindowHome"));

unhandled();

WindowManager.addGlobalCssPath(Config.projectPath("node_modules/@fortawesome/fontawesome-free/css/all.min.css"));

const startApp = () => {
    console.log("Start");
    WindowManager.create(new WindowHome());
}

app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    } else {
        i18nextBackend.clearMainBindings(ipcMain);
    }
});

app.on("activate", () => {
    if (process.platform === "darwin" && BrowserWindow.getAllWindows().length === 0) {
        AgreeLicense.check(startApp);
    }
});

app.whenReady().then(() => {
    const agreeLicense = () => {
        i18nextMainBackend.off("loaded", agreeLicense);
        AgreeLicense.check(startApp);
    }

    i18nextMainBackend.on("loaded", agreeLicense);
});
